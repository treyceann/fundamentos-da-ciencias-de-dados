import requests 
from bs4 import BeautifulSoup
from selenium import webdriver
#BBC News Brasil 
#El país
#Xinhua Português 

def acessar_pagina_selenium():
    navegador = webdriver.Firefox()
    navegador.get("http://portuguese.xinhuanet.com/internacional/index.htm")
    bs = BeautifulSoup(navegador.page_source, "lxml")
    return bs 


def acessar_pagina():
    pagina = requests.get("https://www.gov.br/mre/pt-br/canais_atendimento/imprensa/notas-a-imprensa/notas-a-imprensa")
    #print(pagina.text)
    bs = BeautifulSoup(pagina.text, "html.parser")
    titulo = bs.find("h1").text
    print(titulo) 
    notas_imprensa = bs.find("article")
    print(notas_imprensa.a)

def acessar_pagina_BBC():
    pagina = requests.get("https://www.bbc.com/portuguese/geral-57429249")
    print(pagina.text)
    bs = BeautifulSoup(pagina.text, "html.parser")
    titulo = bs.find("h1").text
    print(titulo) 
    notas_imprensa = bs.find("article")
    print(notas_imprensa.a)

def acessar_pagina_El_pais():
    pagina = requests.get("https://brasil.elpais.com/brasil/2021-06-04/eua-proibem-os-norte-americanos-de-investir-em-59-empresas-da-industria-de-defesa-da-china.html")
    print(pagina.text)
    bs = BeautifulSoup(pagina.text, "html.parser")
    titulo = bs.find("h1").text
    print(titulo) 
    notas_imprensa = bs.find("article")
    print(notas_imprensa.a)

def acessar_pagina_xinhua():
    #pagina = requests.get("http://portuguese.xinhuanet.com/internacional/index.htm")
    #print(pagina.text)
    #bs = BeautifulSoup(pagina.text, "html.parser")
    #noticias = bs.find("ul", {"id": "showData0"})
    bs = acessar_pagina_selenium()
    noticias = bs.find("ul", {"id": "showData0"}).find_all("li")
    #print(noticias)
    for noticia in noticias:
        try:
            titulo = noticia.h3.a.text
            link = noticia.h3.a["href"]
            print(titulo)
            print(link)
            print("#####")
        except:
            pass

def main():
    #acessar_pagina()
    #acessar_pagina_BBC()
    #acessar_pagina_El_pais()
    acessar_pagina_xinhua()

if __name__ == "__main__":
    main()