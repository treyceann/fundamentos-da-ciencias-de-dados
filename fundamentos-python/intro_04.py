def if_elif_else():
    a, b, c = 10, 20, 30
    nome = input("digite um nome")
    nome1 = "Bruna"
    nome2 = "Treyce"
    nome3 = "Gustavo"
    nome4 = "João"
    if nome != nome1:
        print(nome1)
    else:
        print("se não")
    if nome == nome2:
        print(nome2)
    elif nome == nome3:
        print(nome3)
        print("você entrou na segunda condicional")
    elif nome == nome4: 
        print(nome4)
    else:
        print(f'o nome {nome} não tem correpondente')
    #quando uma condição do if/elif é satisfeita, não se consegue rodar a segunda condição


def instrução_return():
    nomes = ["Bruna", "Treyce", "Gustavo", "João"]
    novo_nome = input("Digite um nome")
    for nome in nomes:
        if (nome == "João") or (nome == "Treyce") or (nome == "Gustavo") or (nome == "Bruna"):
            print(f"{nome} já consta na lista")
        if nome != novo_nome: 
            novos.append(novo_nome)
    #print(nomes) 
    nomes = list (set(nomes))
    #print(nomes)
    return nomes  

def input_pais():
    pais = input ("digite um pais: ") 
    pais1 = "Suriname"
    idioma = "neerlandês"
    continente = "América"
    população = "586.634"
    if pais == pais1:
        print(idioma)
    else:
        print("O idioma não é neerlandês")  
    if pais == pais1:
        print(continente)
    else:
        print("O continente não é América")
    if pais == pais1:
        print(população)
    else:
        print("A população não é 586.634")

def if_elif_else_paises():
    pais = input ("digite um pais: ") 
    paises = ["Suriname", "Guiana Francesa", "Inglaterra", "Espanha", "Áustria", "Itália", "Uruguai", "Canadá", "México", "Equador"]
    idioma = ["neerlandês", "francês", "inglês", "espanhol", "alemão", "italiano", "espanhol", "inglês/francês", "espanhol","espanhol"]
    continente = ["América", "América", "Europa", "Europa", "Europa", "Europa", "América", "América", "América", "América"]
    população = ["586.634", "294.071", "55,98 milhões", "47,35 milhões", "8,917 milhões", "59,55 milhões", "3,474 milhões", "38,01 milhões", "128,9 milhões", "17,64 milhões"]
    if pais == paises[0]:
        print(idioma[0])
        print(continente[0])
        print(população[0])
    elif pais == paises[1]:
        print(idioma[1])
        print(continente[1])
        print(população[1])
    elif pais == paises[2]:
        print(idioma[2])
        print(continente[2])
        print(população[2])
    elif pais == paises[3]:
        print(idioma[3])
        print(continente[3])
        print(população[3])
    elif pais == paises[4]:
        print(idioma[4])
        print(continente[4])
        print(população[4])
    elif pais == paises[5]:
        print(idioma[5])
        print(continente[5])
        print(população[5])
    elif pais == paises[6]:
        print(idioma[6])
        print(continente[6])
        print(população[6])
    elif pais == paises[7]:
        print(idioma[7])
        print(continente[7])
        print(população[7])
    elif pais == paises[8]:
        print(idioma[8])
        print(continente[8])
        print(população[8])
    elif pais == paises[9]:
        print(idioma[9])
        print(continente[9])
        print(população[9])
    else:
        print(f'{pais} não está na lista')

def return_paises():
    pais = input ("Digite um novo país: ")
    paises = ["Suriname", "Guiana Francesa", "Inglaterra", "Espanha", "Áustria", "Itália", "Uruguai", "Canadá", "México", "Equador"]
    if pais in paises: 
        #print(pais)
        pass
    elif not pais in paises:
        paises.append(pais)
        #print(paises)
    #print(paises, len paises)
    pais_inserido = pais
    return(paises, pais_inserido)

# def instrução_break():
#     """ utilizado apenas dentro do loop_for
#     interrompe o loop_for e vai para o próximo bloco de código"""
#     paises = return_paises()
#     for pais in paises: 
#         if pais == "Suriname":
#             print("Este país está na lista")
#             break 
#         if pais == "Alemanha":
#             print("Este país não está na lista") #por que não printa esta parte?
#     print("Saiu do loop_for")

def atividade_break():
    info_return_paises = return_paises()
    paises = info_return_paises[0]
    pais_inserido = info_return_paises[1]
    if pais_inserido in paises:
        for pais in ["Suriname", "Guiana Francesa", "Inglaterra", "Espanha", "Áustria", "Itália", "Uruguai", "Canadá", "México", "Equador"]:
            if pais_inserido == "Suriname":
                idioma = "neerlandês"
                continente = "América"
                população = "586.634"
                print(idioma)
                print(continente)
                print(população)
                break
            elif pais_inserido == "Guiana Francesa":
                idioma = "francês"
                continente = "América"
                população = "294.071"
                print(idioma)
                print(continente)
                print(população)
                break
            elif pais_inserido == "Inglaterra":
                idioma = "inglês"
                continente = "Europa"
                população = "55,98 milhões"
                print(idioma)
                print(continente)
                print(população)
                break
            elif pais_inserido == "Espanha":
                idioma = "Espanhol"
                continente = "Europa"
                população = "47,35 milhões"
                print(idioma)
                print(continente)
                print(população)
                break
            elif pais_inserido == "Áustria":
                idioma = "alemão"
                continente = "Europa"
                população = "8,917 milhões"
                print(idioma)
                print(continente)
                print(população)
                break
            elif pais_inserido == "Itália":
                idioma = "italiano"
                continente = "Europa"
                população = "59,55 milhões"
                print(idioma)
                print(continente)
                print(população)
                break
            elif pais_inserido == "Uruguai":
                idioma = "espanhol"
                continente = "América"
                população = "3,474 milhões"
                print(idioma)
                print(continente)
                print(população)
                break
            elif pais_inserido == "Candá":
                idioma = "inglês/francês"
                continente = "América"
                população = "38,01 milhões"
                print(idioma)
                print(continente)
                print(população)
                break
            elif pais_inserido == "México":
                idioma = "espanhol"
                continente = "América"
                população = "128,9 milhões"
                print(idioma)
                print(continente)
                print(população)
                break
            elif pais_inserido == "Equador":
                idioma = "espanhol"
                continente = "América"
                população = "17,64 milhões"
                print(idioma)
                print(continente)
                print(população)
                break
            else: 
                print(f"não há informação sobre o pais {pais_inserido}")
                break
            print("esta linha está dentro do loop_for")
        print("esta linha está fora do loop_for")


def instrução_continue():
    paises = return_paises()
    for pais in paises:
        if pais == "Alemanha":
            continue 
            print("Este país está na lista")
        if pais == "Brasil":
            print("Este país não está na lista")
    print("Saiu do loop_for")

def instrução_try_except():
    paises = return_paises()
    print(idioma)
    try:
        pass
    except:
        pass






def main():
    #if_elif_else()
    #instrução_return()
    #input_pais()
    #return_paises()
    #if_elif_else_paises()
    #instrução_break()
    atividade_break()
    #instrução_continue()
    #instrução_try_except()

if __name__ == "__main__":
    main()